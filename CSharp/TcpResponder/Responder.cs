﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;

namespace TcpResponder
{
    public class Responder
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Responder));

        private readonly int _port;
        private bool _keepGoing = true;

        public Responder(int port)
        {
            _port = port;
        }

        public void Run()
        {
            var myEp = new IPEndPoint(IPAddress.Any, _port);
            var listener = new TcpListener(myEp);
            listener.Start();

            Console.WriteLine("Type any key to exit");

            while (_keepGoing)
            {
                if (!listener.Pending())
                {
                    if (Console.KeyAvailable)
                    {
                        Logger.Info("Exiting");
                        _keepGoing = false;            
                    }
                    else
                        Thread.Sleep(10);
                    continue;
                }

                try
                {
                    var client = listener.AcceptTcpClient();
                    var clientThread = new Thread(ReceiveDataFromClient);
                    clientThread.Start(client);
                }
                catch (Exception ex)
                {
                    Logger.Error($"While try to accept a TCP connection: {ex.Message}");
                }

            }
        }

        private static void ReceiveDataFromClient(object clientObject)
        {
            var client = clientObject as TcpClient;
            if (client == null) return;

            var stream = client.GetStream();

            var buffer = new byte[256];

            var stayConnected = true;

            while (stayConnected)
            {
                try
                {
                    var bytesRead = stream.Read(buffer, 0, buffer.Length);
                    if (bytesRead <= 0) continue;

                    var data = System.Text.Encoding.Unicode.GetString(buffer, 0, bytesRead);
                    Console.WriteLine($"Received {bytesRead} bytes: {data}\n");
                    Logger.Debug($"Received {bytesRead} bytes: {data}");
                    Thread.Sleep(100);
                }
                catch (IOException ex)
                {
                    Logger.Error($"While trying to read: {ex.Message}");
                    stayConnected = false;
                }
            }

            Logger.Info($"Closing TcpClient");
            client.Close();
        }

    }
}
