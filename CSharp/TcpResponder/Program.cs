﻿using log4net.Config;

namespace TcpResponder
{
    public class Program
    {
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var port = 15000;
            if (args.Length > 0)
            {
                int.TryParse(args[0], out port);
            }

            var responder = new Responder(port);
            responder.Run();
        }

    }
}
