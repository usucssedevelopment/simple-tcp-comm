﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;

namespace TcpInitiator
{
    public class Initiator
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Initiator));

        public int ProcessId { get; set; }

        public void Run(string messageToSend, int repeatCount, IPEndPoint targetEndPoint, int sleepTime)
        {
            // Create a TcpClient
            var client = new TcpClient();

            // Connect the client to the server -- remember that TCP is connection orient
            client.Connect(targetEndPoint);

            // Note that previous two statement can be combined using one of the
            // TcpClient constructs:
            //
            //  TcpClient client = new TcpClient("127.0.0.1", 15000);
            //
            // IMPORTANT: This constructor is NOT the same as one the takes an IPEndPoint
            // as a parameter.  That constructor binds the TcpClient to a local end point;
            // it does not connect the client to a remote end point.

            var stream = client.GetStream();
            var bytes = System.Text.Encoding.Unicode.GetBytes($"{messageToSend} ({ProcessId}). ");

            try
            {
                for (var i = 0; i < repeatCount; i++)
                {
                    stream.Write(bytes, 0, bytes.Length);
                    if (sleepTime > 0)
                        Thread.Sleep(sleepTime);
                }
            }
            catch (Exception ex)
            {
                Logger.Error($"While sending: {ex.Message}");
            }


            // Give the responder time to read the data on its end
            Thread.Sleep(1000);
            client.Close();
        }

    }
}
