﻿using System.Net;

namespace TcpResponder
{
    public static class EndPointParser
    {
        public static IPEndPoint Parse(string hostnameAndPort)
        {
            if (string.IsNullOrWhiteSpace(hostnameAndPort)) return null;

            IPEndPoint result = null;
            var tmp = hostnameAndPort.Split(':');
            if (tmp.Length == 2 && !string.IsNullOrWhiteSpace(tmp[0]) && !string.IsNullOrWhiteSpace(tmp[1]))
                result = new IPEndPoint(ParseAddress(tmp[0]), ParsePort(tmp[1]));

            return result;
        }

        private static IPAddress ParseAddress(string hostname)
        {
            IPAddress result = null;
            var addressList = Dns.GetHostAddresses(hostname);
            for (var i = 0; i < addressList.Length && result == null; i++)
                if (addressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    result = addressList[i];
            return result;
        }

        private static int ParsePort(string portStr)
        {
            var port = 0;
            if (!string.IsNullOrWhiteSpace(portStr))
                int.TryParse(portStr, out port);
            return port;
        }
    }

}
