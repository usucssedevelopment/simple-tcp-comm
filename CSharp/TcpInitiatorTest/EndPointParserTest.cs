﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TcpInitiatorTest
{
    [TestClass]
    public class EndPointParserTest
    {
        [TestMethod]
        public void EndPointParser_TestWithIpAddressAndPort()
        {
        }

        [TestMethod]
        public void EndPointParser_TestWithHostNameAndPort()
        {
        }

        [TestMethod]
        public void EndPointParser_TestWithNoPort()
        {
        }

        [TestMethod]
        public void EndPointParser_TestWithNoHost()
        {
        }

        [TestMethod]
        public void EndPointParser_TestWithOtherBadInput()
        {
        }

    }
}
