﻿using System;
using System.Collections.Generic;
using log4net.Config;
using TcpResponder;

namespace TcpInitiator
{
    public class Program
    {

        private static readonly List<string> StandardMessages = new List<string>()
        {
            "Nothing specific to say",
            "Hi there",
            "Hello",
            "What's up",
            "Good day",
            "Gutten Tag",
            "Hey"
        };
        private static readonly Random Random = new Random();

        /// <summary>
        /// Usage:
        ///     Be sure the TcpResponder is running
        /// 
        ///     TcpInitiator ["Some message" [repeat count]]
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            var argIndex = 0;
           
            string messageToSend;
            if (args.Length > argIndex)
            {
                messageToSend = args[argIndex];
                argIndex++;
            }
            else
            {
                var index = Random.Next(StandardMessages.Count);
                messageToSend = StandardMessages[index];
            }

            var repeatCount = 100;
            if (args.Length > argIndex)
            {
                int.TryParse(args[argIndex], out repeatCount);
                argIndex++;
            }

            var target = "127.0.0.1:15000";
            if (args.Length > argIndex)
            {
                target = args[1];
                argIndex++;
            }
            var targEndPoint = EndPointParser.Parse(target);

            var sleepTime = 1000;
            if (args.Length > argIndex)
            {
                int.TryParse(args[argIndex], out sleepTime);
                argIndex++;
            }

            var processId = Random.Next(32000);
            if (args.Length > argIndex)
            {
                int.TryParse(args[argIndex], out sleepTime);
            }

            var initiator = new Initiator() {ProcessId = processId};
            initiator.Run(messageToSend, repeatCount, targEndPoint, sleepTime);
        }
    }
}
